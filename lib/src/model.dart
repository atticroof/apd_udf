import 'package:flutter/foundation.dart';

@immutable
abstract class Mutator<T> {
  void call(T state);
}

class MutableModel<T> extends ChangeNotifier implements ValueListenable<T> {
  MutableModel(this.value);

  @override
  final T value;

  void apply(Mutator<T> mutator) {
    mutator(value);
    notifyListeners();
  }
}

typedef Reducer<T> = T Function(T);

class Model<T> extends ChangeNotifier implements ValueListenable<T> {
  Model(this._state);

  @override
  T get value => _state;
  T _state;

  void reduce(Reducer<T> reducer) {
    _state = reducer(_state);
    notifyListeners();
  }
}
