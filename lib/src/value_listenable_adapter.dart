import 'package:flutter/foundation.dart';

class ValueListenableAdapter<A, T> implements ValueListenable<T> {
  final ValueListenable<A> listenable;
  final T Function(A) adapter;

  const ValueListenableAdapter({
    required this.listenable,
    required this.adapter,
  });

  @override
  void addListener(listener) => listenable.addListener(listener);

  @override
  void removeListener(listener) => listenable.removeListener(listener);

  @override
  T get value => adapter(listenable.value);
}

class ValueListenableAdapter2<A, B, T> implements ValueListenable<T> {
  final ValueListenable<A> listenable1;
  final ValueListenable<B> listenable2;
  final T Function(A, B) adapter;

  const ValueListenableAdapter2({
    required this.listenable1,
    required this.listenable2,
    required this.adapter,
  });

  @override
  void addListener(listener) {
    listenable1.addListener(listener);
    listenable2.addListener(listener);
  }

  @override
  void removeListener(listener) {
    listenable1.removeListener(listener);
    listenable2.removeListener(listener);
  }

  @override
  T get value => adapter(
        listenable1.value,
        listenable2.value,
      );
}

class ValueListenableAdapter3<A, B, C, T> implements ValueListenable<T> {
  final ValueListenable<A> listenable1;
  final ValueListenable<B> listenable2;
  final ValueListenable<C> listenable3;
  final T Function(A, B, C) adapter;

  const ValueListenableAdapter3({
    required this.listenable1,
    required this.listenable2,
    required this.listenable3,
    required this.adapter,
  });

  @override
  void addListener(listener) {
    listenable1.addListener(listener);
    listenable2.addListener(listener);
    listenable3.addListener(listener);
  }

  @override
  void removeListener(listener) {
    listenable1.removeListener(listener);
    listenable2.removeListener(listener);
    listenable3.removeListener(listener);
  }

  @override
  T get value => adapter(
        listenable1.value,
        listenable2.value,
        listenable3.value,
      );
}

class ValueListenableAdapter4<A, B, C, D, T> implements ValueListenable<T> {
  final ValueListenable<A> listenable1;
  final ValueListenable<B> listenable2;
  final ValueListenable<C> listenable3;
  final ValueListenable<D> listenable4;
  final T Function(A, B, C, D) adapter;

  const ValueListenableAdapter4({
    required this.listenable1,
    required this.listenable2,
    required this.listenable3,
    required this.listenable4,
    required this.adapter,
  });

  @override
  void addListener(listener) {
    listenable1.addListener(listener);
    listenable2.addListener(listener);
    listenable3.addListener(listener);
    listenable4.addListener(listener);
  }

  @override
  void removeListener(listener) {
    listenable1.removeListener(listener);
    listenable2.removeListener(listener);
    listenable3.removeListener(listener);
    listenable4.removeListener(listener);
  }

  @override
  T get value => adapter(
        listenable1.value,
        listenable2.value,
        listenable3.value,
        listenable4.value,
      );
}
