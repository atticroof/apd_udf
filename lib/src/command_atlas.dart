import 'dart:core';

import 'package:flutter/widgets.dart';

typedef CommandBuilder<T> = Command<T> Function();

@immutable
abstract class Event<E> {
  final E name;
  Event(this.name);
}

@immutable
abstract class Command<E> {
  final CommandAtlas atlas;
  Command(this.atlas);
  Future execute(E event);
}

class CommandAtlas {
  final _maps = <Type, CommandMap>{};
  CommandMap<E> findMap<E>() => (_maps[E] ??= CommandMap<E>._()) as CommandMap<E>;
}

class CommandMap<E> {
  CommandMap._();

  final _byValue = <E, Set<CommandBuilder<E>>>{};
  final _byType = <Type, Map<E, Set<CommandBuilder<Event<E>>>>>{};

  Set<CommandBuilder<E>> _setByValue(E e) => _byValue[e] ??= <CommandBuilder<E>>{};

  Map<E, Set<CommandBuilder<T>>> _mapByType<T extends Event<E>>() {
    return (_byType[T] ??= <E, Set<CommandBuilder<T>>>{}) as Map<E, Set<CommandBuilder<T>>>;
  }

  Set<CommandBuilder<T>> _setByName<T extends Event<E>>(E n) {
    return _mapByType<T>()[n] ??= <CommandBuilder<T>>{};
  }

  void mapByName<T extends Event<E>>(E name, CommandBuilder<T> builder) => _setByName<T>(name);
  void mapByValue(E event, CommandBuilder<E> builder) => _setByValue(event).add(builder);

  void unmap(E event, CommandBuilder builder) {
    if (_byValue[event] != null) _byValue[event]!.remove(builder);
    _byType.forEach((_, value) {
      if (value[event] != null) value[event]!.remove(builder);
    });
  }

  Future _dispatchByValue(E value) async {
    for (CommandBuilder<E> builder in _setByValue(value)) {
      await builder().execute(value);
    }
  }

  Future _dispatchByName<T extends Event<E>>(E value, T event) async {
    for (CommandBuilder<T> builder in _setByName<T>(value)) {
      await builder().execute(event);
    }
  }

  Future dispatchEvent<T extends Event<E>>(E value, [T? event]) async {
    if (event == null) return _dispatchByValue(value);
    return _dispatchByName<T>(value, event);
  }
}
