import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

typedef ValueSelector<A, T> = T Function(A);
typedef ShouldRebuild<T> = bool Function(T old, T value);

class Subscriber<A, T> extends StatefulWidget {
  final ValueListenable<A> _listenable;
  final ValueWidgetBuilder<T> builder;
  final ValueSelector<A, T> _selector;
  final ShouldRebuild<T>? _shouldRebuild;
  final Widget? child;

  Subscriber({
    Key? key,
    required this.builder,
    required ValueListenable<A> listenable,
    required ValueSelector<A, T> selector,
    ShouldRebuild<T>? shouldRebuild,
    this.child,
  })  : _shouldRebuild = shouldRebuild,
        _selector = selector,
        _listenable = listenable,
        super(key: key);

  @override
  State<StatefulWidget> createState() => _SubscriberState<A, T>();
}

class _SubscriberState<A, T> extends State<Subscriber<A, T>> {
  late T _value;
  late ValueSelector<A, T> _selector;
  late ValueListenable<A> _listenable;
  ShouldRebuild<T>? _shouldRebuild;

  @override
  void initState() {
    super.initState();
    _selector = widget._selector;
    _listenable = widget._listenable;
    _value = _selector(_listenable.value);
    _shouldRebuild = widget._shouldRebuild;
    _listenable.addListener(_valueChanged);
  }

  @override
  void didUpdateWidget(Subscriber<A, T> oldWidget) {
    if (oldWidget._listenable != widget._listenable) {
      _listenable.removeListener(_valueChanged);
      _listenable = widget._listenable;
      _listenable.addListener(_valueChanged);
    }

    _shouldRebuild = widget._shouldRebuild;
    _selector = widget._selector;
    _value = _selector(_listenable.value);

    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    _listenable.removeListener(_valueChanged);
    super.dispose();
  }

  void _valueChanged() {
    final _v = _selector(_listenable.value);

    if (_shouldRebuild != null ? _shouldRebuild!(_value, _v) : _value != _v) setState(() => _value = _v);
  }

  @override
  Widget build(BuildContext context) {
    return widget.builder(context, _value, widget.child);
  }
}
